--create database DBDay2
--create table artis
--(id int primary key identity(1,1),
--kd_artis varchar(100) not null,
--nm_artis varchar (100) not null,
--kelamin varchar (100) not null,
--bayaran decimal (18,4) not null,
--award int not null,
--negara varchar (100)
--)
--select * from artis


--select * from artis
--insert into artis
--(kd_artis,nm_artis,kelamin,bayaran,award,negara) values
----('A001','ROBERT DOWNEY JR','PRIA','3000000000','2','AS')
--('A002','ANGELINA JOLIE','WANITA','7000000000','1','AS'),
--('A003','JACKIE CHAN','PRIA','2000000000','7','HK'),
--('A004','JOE TASLIM','PRIA','3500000000','1','ID'),
--('A005','CHELSEA ISLAN','WANITA','3000000000','0','ID')

UPDATE artis set bayaran = '350000000' where kd_artis ='A005'


--create table film
--(
--id int primary key identity(1,1),
--kd_film varchar (10) not null,
--nm_film varchar (55) not null,
--genre varchar (55) not null,
--artis varchar (55) not null,
--produser varchar (55) not null,
--pendapatan decimal (18,4) not null,
--nominal int not null

--)
--select * from film
--insert into film
--(kd_film,nm_film,genre,artis,produser,pendapatan,nominal) values
----('F001','IRON MAN','0001','A001','PD01','2000000000','3')
--('F002','IRON MAN 2','0001','A001','PD01','1800000000','2'),
--('F003','IRON MAN 3','0001','A001','PD01','1200000000','0'),
--('F004','AVENGER : CIVIL WAR','0001','A001','PD01','2000000000','1'),
--('F005','SPIDERMAN HOME COMING','0001','A001','PD01','2000000000','3'),
--('F006','THE RAID','0001','A004','PD03','800000000','5'),
--('F007','FAST & FURIOUS','0001','A004','PD05','830000000','2'),
--('F008','HABIBIE DAN AINUN','0004','A005','PD03','670000000','4'),
--('F009','POLICE STORY','0001','A003','PD02','700000000','3'),
--('F010','POLICE STROY 2','0001','A004','PD02','710000000','1'),
--('F011','POLICE STROY 3','0001','A004','PD02','615000000','0'),
--('F012','RUSH HOUR','0003','A003','PD05','695000000','2'),
--('F013','KUNGFU PANDA','0003','A003','PD05','923000000','5')


--UPDATE film set artis = 'A003' where kd_film ='F011'
--UPDATE film set pendapatan = '1300000000' where kd_film ='F005'
--UPDATE film set nominal = '0' where kd_film ='F005'
--create table produser
--(id int primary key identity(1,1),
--kd_produser varchar(50) not null,
--nm_produser varchar (50) not null,
--international varchar (100) not null
--)
--select * from produser
--insert into produser
--(kd_produser,nm_produser,international) values
--('PD01','MARVEL','YA'),
--('PD02','HONGKONG CINEMA','YA'),
--('PD03','RAPI FILM','TIDAK'),
--('PD04','PARKIT','TIDAK'),
--('PD05','PARAMOUNT PICTURE','YA')
--drop table produser
--create table negara
--(id int primary key identity(1,1),
--kd_negara varchar(100) not null,
--nm_negara varchar (100) not null
--)
--select * from negara
--insert into negara
--(kd_negara,nm_negara) values
--('AS','AMERIKA SERIKAT'),
--('HK','HONGKONG'),
--('ID','INDONESIA'),
--('IN','INDIA')

--create table genre
--(id int primary key identity(1,1),
--kd_genre varchar(50) not null,
--nm_genre varchar (50) not null
--)
--select * from genre

--insert into genre
--(kd_genre,nm_genre) values
--('0001','ACTION'),
--('0002','HORROR'),
--('0003','COMEDY'),
--('0004','DRAMA'),
--('0005','THRILLER'),
--('0006','FICTION')

--drop table produser

--create table produser
--(id int primary key identity(1,1),
--kd_produser varchar(50) not null,
--nm_produser varchar (50) not null,
--international varchar (100) not null
--)

--select * from produser
--insert into produser
--(kd_produser,nm_produser,international) values
--('PD01','MARVEL','YA'),
--('PD02','HONGKONG CINEMA','YA'),
--('PD03','RAPI FILM','TIDAK'),
--('PD04','PARKIT','TIDAK'),
--('PD05','PARAMOUNT PICTURE','YA')

select  sum(fil.pendapatan) as pendapatan , pro.nm_produser 
from film as fil												--1
join produser as pro
on fil.produser = pro.kd_produser
group by pro.nm_produser
having pro.nm_produser = 'MARVEL'

--select nm_film,nominal from film where nominal ='0'    --2
--select top 1 nm_film, pendapatan from film    --3


--select nm_film from film where nm_film like 'P%'   --4
--select nm_film from film where nm_film like '%y' --5
--select nm_film from film where nm_film like '%D%' --6

--select fil.nm_film, art.nm_artis from film as fil  --7
--join artis as art
--on fil.artis = art.kd_artis

--select fil.nm_film, art.negara  from negara as neg		--8
--join artis as art on neg.kd_negara = art.negara
--join film as fil on fil.artis = art.kd_artis
--where art.negara='HK'

--select fil.nm_film, neg.nm_negara from negara as neg			--9
--join artis as art on neg.kd_negara = art.negara
--join film as fil on fil.artis = art.kd_artis
--where neg.nm_negara not like '%o%'

--select nm_artis from film as fil
--right join artis as art on fil.artis = art.kd_artis				--10
--where fil.artis is null 

--select art.nm_artis,gen.nm_genre from film as fil				--11
--join genre as gen on gen.kd_genre =fil.genre
--join artis as art on fil.artis = art.kd_artis
--where gen.nm_genre ='DRAMA'

--select art.nm_artis,gen.nm_genre from film as fil				--12
--join genre as gen on gen.kd_genre = fil.genre
--join artis as art on fil.artis = art.kd_artis
--group by art.nm_artis,gen.nm_genre
--having gen.nm_genre = 'ACTION'

--select neg.kd_negara,neg.nm_negara, count(fil.nm_film) as jumlah_Files		--13
--from film as fil
--join artis as art on art.kd_artis = fil.artis
--right join negara as neg on neg.kd_negara = art.negara
--group by neg.kd_negara,neg.nm_negara

--select nm_film													--14
--from film as fil
--join produser as pro on pro.kd_produser=fil.produser
--where pro.international = 'YA'

--select nm_produser, count (fil.nm_film) as jumlah_film		--15
--from film as fil
--right join produser as pro on pro.kd_produser = fil.produser
--group by nm_produser



select *,
	case	
		when bayaran >= 500000000 then bayaran*0.2
		when bayaran <500000000 and bayaran >200000000 then bayaran*0.1
		else 0
	end as pajak,
	case	
		when bayaran >= 500000000 then bayaran -(bayaran * 0.2) 
		when bayaran <500000000 and bayaran >200000000 then bayaran -(bayaran * 0.1) 
		else bayaran
	
	end as Total_Bayaran
	from artis


select kd_artis +' ' + nm_artis from artis
select Concat(kd_artis,' ',nm_artis) from artis
select kd_artis +' ' + nm_artis as from artis



select tanggal_mulai, getdate() as tgl_sekarang,
DATEDIFF (YEAR, tanggal_mulai,GETDATE()) as umur from TblCuti

select GETDATE()