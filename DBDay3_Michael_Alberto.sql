--create database DBDay3
--drop table karyawan
--create table karyawan
--(id bigint primary key identity (1,1) not null,
--nip varchar (50) not null,
--nama_depan varchar (50) not null,
--nama_belakang varchar (50) not null,
--jenis_kelamin varchar (50) not null,
--agama varchar (50) not null,
--tempat_lahir varchar (50) not null,
--tgl_lahir date ,
--alamat varchar (100) not null,
--pendidikan_terakhir varchar(50) not null,
--tgl_masuk date
--)
--select * from karyawan
--insert into karyawan
--(nip,nama_depan,nama_belakang,jenis_kelamin,agama,tempat_lahir,tgl_lahir,alamat,pendidikan_terakhir,tgl_masuk) values
--('001','Hamidi','Samsudin','Pria','Islam','Sukabumi','1977-04-21','Jl. Sudirman No.12','S1 Teknik Mesin','2016-12-07'),
--('003','Paul','Christian','Pria','Kristen','Ambon','1980-05-27','Jl. Veteran No.4','S1 Pendidikan Geografi','2014-01-12'),
--('002','Ghandi','Wamida','Wanita','Islam','Palu','1992-01-12','Jl. Rambutan No.22','SMA Negeri 02 Palu','2014-12-01')
--drop table divisi
--select * from jabatan
--create table divisi
--(id bigint primary key identity (1,1) not null,
--kd_divisi varchar (50) not null,
--nama_divisi varchar (50) not null
--)
--select * from divisi
--insert into divisi
--(kd_divisi,nama_divisi) values
--('GD','Gudang'),
--('HRD','HRD'),
--('KU','Keuangan'),
--('UM','Umum')

--drop table jabatan
--create table jabatan
--(id bigint primary key identity (1,1) not null,
--kd_jabatan varchar(50) not null,
--nama_jabatan varchar (50) not null,
--gaji_pokok numeric,
--tunjangan_jabatan numeric
--)
--select * from jabatan
--insert into jabatan
--(kd_jabatan,nama_jabatan,gaji_pokok,tunjangan_jabatan) values
--('MGR','Manager','5500000','1500000'),
--('OB','Office Boy','1900000','200000'),
--('ST','Staff','3000000','750000'),
--('WMGR','Wakil Manager','4000000','1200000')

--create table pekerjaan
--(id bigint primary key identity (1,1) not null,
--nip varchar (50) not null,
--kode_jabatan varchar (50) not null,
--kode_divisi varchar (50) not null,
--tunjangan_kinerja numeric,
--kota_penempatan varchar (50)
--)

--select * from pekerjaan
--insert into pekerjaan
--(nip,kode_jabatan,kode_divisi,tunjangan_kinerja,kota_penempatan) values
----('001','ST','KU','750000','Cianjur')
--('002','OB','UM','350000','Sukabumi'),
--('003','MGR','HRD','1500000','Sukabumi')

--ini_nomor_1
--select Concat (kar.nama_depan, kar.nama_belakang)as nama_lengkap, jab.nama_jabatan, jab.gaji_pokok + jab.tunjangan_jabatan as gaji_tunjangan from karyawan as kar
--join pekerjaan as pek on pek.nip = kar.nip
--join jabatan as jab on jab.kd_jabatan=pek.kode_jabatan
--where jab.gaji_pokok + jab.tunjangan_jabatan < 5000000

--ini_nomor_2
--select concat (kar.nama_depan,' ' ,kar.nama_belakang) as nama_lengkap, 
--jab.nama_jabatan, 
--div.nama_divisi,
--jab.gaji_pokok +jab.tunjangan_jabatan + pek.tunjangan_kinerja as total_gaji, 
--0.05 * (jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja) as pajak, 
--jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja -(0.05 *(jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja))as gaji_bersih 
--from karyawan as kar
--join pekerjaan as pek on pek.nip = kar.nip
--join jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
--join divisi as div on div.kd_divisi = pek.kode_divisi
--where kar.jenis_kelamin = 'Pria' and pek.kota_penempatan != 'Sukabumi'


--nomor_3
--select kar.nip,
--concat (kar.nama_depan,' ',kar.nama_belakang)as nama_lengkap,
--jab.nama_jabatan,
--div.nama_divisi,
--0.25 *((jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja)*7) as bonus
--from karyawan as kar
--join pekerjaan as pek on pek.nip = kar.nip
--join jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
--join divisi as div on div.kd_divisi = pek.kode_divisi

--nomor_4
--select kar.nip,
--concat(kar.nama_depan,' ',kar.nama_belakang) as nama_lengkap,
--jab.nama_jabatan,
--div.nama_divisi,
--jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja as total_gaji,
--0.05 * (jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja) as infak
--from karyawan as kar
--join pekerjaan as pek on pek.nip = kar.nip
--join jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
--join divisi as div on div.kd_divisi = pek.kode_divisi
--where pek.kode_jabatan = 'MGR'


--nomor_5
--select kar.nip,
--concat (kar.nama_depan,' ',kar.nama_belakang) as nama_lengkap,
--nama_jabatan,
--kar.pendidikan_terakhir,
--'2000000' as tunjangan_pendidikan,
--jab.gaji_pokok + jab.tunjangan_jabatan + 2000000 as total_gaji
--from karyawan as kar
--join pekerjaan as pek on pek.nip = kar.nip
--join jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
--Where kar.pendidikan_terakhir like 'S1%' 
--order by kar.nip asc


--nomor_6
--select kar.nip,
--concat (kar.nama_depan,' ',kar.nama_belakang) as nama_lengkap,
--jab.nama_jabatan,
--div.nama_divisi,
--case
--	when jab.kd_jabatan='MGR' then 0.25 * ((jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja)*7)
--	when jab.kd_jabatan='ST' then 0.25 * ((jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja )*5)
--	else 0.25 * ((jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja)*2)
--end as bonus
--from karyawan as kar
--join pekerjaan as pek on pek.nip = kar.nip
--join jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
--join divisi as div on div.kd_divisi = pek.kode_divisi

--7. konvert kolom ditabel tertentu menjadi kolom unique
--alter table karyawan
--add constraint uc_karyawan unique (nip)

--8.convert uniq index ke on a table
create unique index nipp
on karyawan (nip)

--9
--select concat(kar.nama_depan,' ', upper(kar.nama_belakang))
--as nama_lengkap
--from karyawan as kar
--where nama_belakang like 'W%'

--10
--select concat(kar.nama_depan,' ',kar.nama_belakang)as nama_lengkap,
--kar.tgl_masuk,
--jab.nama_jabatan,
--div.nama_divisi,
--jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja as total_gaji,
--case
--	when datediff (YEAR,kar.tgl_masuk,GETDATE()) > 8 
--	then (0.1 * (jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja)) 
--	else (jab.gaji_pokok + jab.tunjangan_jabatan + pek.tunjangan_kinerja)
--end bonus,
--datediff (YEAR,kar.tgl_masuk,GETDATE()) as lama_kerja
--from karyawan as kar
--join pekerjaan as pek on pek.nip = kar.nip
--join jabatan as jab on jab.kd_jabatan = pek.kode_jabatan
--join divisi as div on div.kd_divisi = pek.kode_divisi
--where datediff (YEAR,kar.tgl_masuk,GETDATE()) >= 8










