--create database DBDay4
--create table salesperson
--(
--id int primary key identity (1,1),
--nama varchar (50) not null,
--bod date,
--salary decimal (18,2)
--)
--insert into salesperson
--(nama,bod,salary) values
--('Abe','9-11-1988','140000'),
--('Bob','9-11-1978','44000'),
--('Chris','9-11-1983','40000'),
--('Dan','9-11-1980','52000'),
--('Ken','9-11-1977','115000'),
--('Joe','9-11-1990','38000')
--create table orders
--(
--id int primary key identity (1,1),
--order_date date,
--cust_id int,
--salesperson_id int,
--amount decimal(18,2)
--)
--insert into orders
--(order_date,cust_id,salesperson_id,amount) values
--('8-2-2020','4','2','540'),
--('1-22-2021','4','5','1800'),
--('7-14-2019','9','1','460'),
--('1-29-2018','7','2','2400'),
--('2-3-2021','6','4','600'),
--('3-2-2020','6','4','720'),
--('5-6-2021','9','4','150')

--nomor1. Informasi nama sales yang memiliki order lebih dari 1. sudah..
--select sal.nama,ord.salesperson_id,
--count(ord.salesperson_id) as jmlDiaNgeOrder
--from orders as ord
--join salesperson as sal on sal.id = ord.salesperson_id
--group by sal.nama,ord.salesperson_id
--having count(ord.salesperson_id)>1


--nomor2  Informasi nama sales yang total amount ordernya di atas 1000. sudah..
--select sal.nama,ord.salesperson_id,
--sum(amount) as jmlDuitOrderan
--from orders as ord
--right join salesperson as sal on sal.id = ord.salesperson_id
--group by sal.nama,ord.salesperson_id
--having sum(amount) >1000

--nomor3 Informasi nama sales, umur, gaji dan total amount order yang tahun 
--ordernya >= 2020 
--dan data ditampilan berurut sesuai dengan umur (ascending). sudah..

--select sal.nama,
--datediff (year,sal.bod,getdate()) as umur,
--sal.salary,
--sum(amount) as jmlDuitOrderanDiatasAtauSamaDengantahun2020
--from orders as ord
--right join salesperson as sal on sal.id = ord.salesperson_id
--where year (ord.order_date) >= 2020
--group by sal.nama,datediff (year,sal.bod,getdate()),sal.salary
--order by datediff (year,sal.bod,getdate()) asc


--4. Carilah rata-rata total amount masing-masig sales urutkan 
--dari hasil yg paling besar sudah..

--select sal.nama,
--case
--	when AVG(amount) is null then  0
--	else AVG(amount)
--end as rataan_amount
--from orders as ord
--right join salesperson as sal on sal.id = ord.salesperson_id
--group by sal.nama,ord.salesperson_id
--order by AVG(amount) desc

--nomor 5 perusahaan akan memberikan bonus bagi sales yang berhasil memiliki order 
--lebih dari 2 dan total order lebih dari 1000 sebanyak 30% dari salary   sudah..

--select sal.nama,ord.salesperson_id,
--count(ord.salesperson_id) as jmlDiaNgeOrder,
--sum(amount) as jmlDuitOrderan,
--case 
--	when sum(amount) >1000 and count(ord.salesperson_id)>2 then 0.3 * sal.salary
--	else 0
--end as bonus,
--sal.salary * 1.3 as totalGajiDanBonus
--from orders as ord
--right join salesperson as sal on sal.id = ord.salesperson_id
--group by sal.nama,ord.salesperson_id,sal.salary
--having count(ord.salesperson_id)>2

--nomor 6 Tampilkan data sales yang belum memiliki orderan sama sekali sudah..

--select sal.nama,
--count(ord.salesperson_id) as jmlDiaNgeOrder
--from orders as ord
--right join salesperson as sal on sal.id = ord.salesperson_id
--where ord.salesperson_id is null
--group by sal.nama

--nomor 7 Gaji sales akan dipotong jika tidak memiliki orderan,  gaji akan di potong sebanyak 2% sudah...
select sal.nama,
sal.salary,
count(ord.salesperson_id) as jmlDiaNgeOrder,

case 
	when count(ord.salesperson_id) > 0 then sal.salary
	else sal.salary * 0.98
end as gajiSetelahPotong
from orders as ord
right join salesperson as sal on sal.id = ord.salesperson_id

group by sal.nama,ord.salesperson_id,sal.salary




