﻿using System;

namespace LogicDay1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Biodata();
            /*Console.Write("nilai x = ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("nilai y = ");
            int b = int.Parse(Console.ReadLine());
            int hasilJumlah = Penjumlahan(a,b);
            Console.WriteLine($"{a}+{b}={hasilJumlah}");*/

            //Soal 1
            PemulungRokok();
            int n;

            Modulus();

            LuasKelPersegi();

            LuasKelLing();




            Console.ReadKey();

        }



        static void Biodata()
        {
            Console.WriteLine("Hello World!");
            Console.WriteLine("BIODATA SAYA");
            Console.WriteLine("Nama = Michael Alberto Ferdinan");
            Console.WriteLine("Umur = 24");
            Console.WriteLine("Alamat = Bandung");
            Console.WriteLine("Kampus = Upi");
            Console.WriteLine("Jurusan = Matematika");
            Console.WriteLine("--------------------------------------------------");

            Console.Write("Masukan nama Anda = ");
            string namaanda = Console.ReadLine();

            int tahunskrng = 2022;
            int bulanskrng = 6;
            int tahunlahir, umur, bulanlahir;

            Console.Write("Masukan tahun lahir Anda = ");
            tahunlahir = int.Parse(Console.ReadLine());

            Console.Write("Masukan bulan lahir Anda = ");
            bulanlahir = int.Parse(Console.ReadLine());
            if (bulanlahir <= bulanskrng)
            {
                umur = tahunskrng - tahunlahir;
            }
            else
            {
                umur = tahunskrng - tahunlahir - 1;
            }
            Console.Write("Masukan kampus Anda = ");
            string kampus = Console.ReadLine();


            Console.Write("Masukan jurusan Anda = ");
            string jurusan = Console.ReadLine();

            Console.Write("Masukan alamat Anda = ");
            string alamat = Console.ReadLine();

            Console.WriteLine("");

            Console.WriteLine($"Hallo, {namaanda} selamat datang di bootcamp batch 295", namaanda);
            Console.WriteLine($"Umur = {umur}", umur);
            Console.WriteLine($"Alamat = {alamat}", alamat);
            Console.WriteLine($"Kampus = {kampus}", kampus);
            Console.WriteLine($"Jurusan = {jurusan}", jurusan);




            
        }

        static int Penjumlahan(int x,int y)
        {
            /*int x = 20;
            int y = 10;*/
            int hasil;
            
            hasil = x + y;
            Console.WriteLine($"{x}+{y}={hasil}");
            return hasil;
        }

        static void LuasKelLing()
        {
            const double pi = 22/7;
            double luaslingkaran, kelilinglingkaran;
            Console.Write("nilai r = ");
            int r = int.Parse(Console.ReadLine());
            
            
            kelilinglingkaran = 2 * r * pi;
            luaslingkaran = pi * r * r;
            Console.WriteLine($"Luas Lingkaran={luaslingkaran}");
            Console.WriteLine($"Keliling Lingkaran ={kelilinglingkaran}");


        }

        static void LuasKelPersegi()
        {
            int luaspersegi, kelilingpersegi;
            Console.Write("nilai s = ");
            int s = int.Parse(Console.ReadLine());
            luaspersegi = s * s;
            kelilingpersegi = 4 * s;
            Console.WriteLine($"Luas Persegi={luaspersegi}");
            Console.WriteLine($"Keliling Persegi={kelilingpersegi}");
            
        }

        static void Modulus()
        {
            //c mod d
            int c, d,sisa;
            Console.Write("nilai c = ");
            c = int.Parse(Console.ReadLine());
            Console.Write("nilai d = ");
            d = int.Parse(Console.ReadLine());
            sisa = c % d;
            if (sisa != 0)
            {
                Console.WriteLine($"{c}%{d}={sisa}");
            }
            else
            {
                Console.WriteLine($"{c}%{d}={0}");
            }
                
        }
        
        static void PemulungRokok()
        {
            //n itu puntung rokok
            Console.Write("nilai n = ");
            int n = int.Parse(Console.ReadLine());
            int sisapuntung = n % 8;
            int totalrokok = n / 8;
            int penghasilanrokok = totalrokok * 500;
            Console.WriteLine($"{totalrokok}");
            Console.WriteLine($"{penghasilanrokok}");

        }

    }
}
