﻿using System;

namespace LogicDay3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("1- berapaaja = ");
            pilih = int.Parse(Console.ReadLine());
            switch (pilih)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal6();
                    break;
                case 7:
                    soal7();
                    break;
                case 8:
                    soal8();
                    break;
                case 9:
                    soal9();
                    break;
                case 10:
                    soal10();
                    break;
            }



        }
        static void soal1()
        {

            int n = 7;

            int x = 1;
            Console.Write($"1 ");

            for (int i = 1; i < n; i++)
            {
                x += 2;

                Console.Write($"{x} ");

            }


        }
        static void soal2()
        {
            int n = 7;

            int x = 0;


            for (int i = 1; i <= n; i++)
            {
                x += 2;

                Console.Write($"{x} ");

            }
        }
        static void soal3()
        {
            int n = 7;

            int x = 1;
            Console.Write($"1 ");

            for (int i = 1; i < n; i++)
            {
                x += 3;

                Console.Write($"{x} ");

            }
        }
        static void soal4()
        {
            int n = 7;

            int x = 1;


            for (int i = 1; i < n; i++)
            {
                Console.Write($"{x} ");
                x += 4;



            }
        }
        static void soal5()
        {
            int n = 7;

            int x = 1;


            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write($"* ");
                }
                else
                {
                    Console.Write($"{x} ");
                    x += 4;


                }


            }
        }
        static void soal6()
        {
            int n = 7;

            int x = 1;


            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write($"* ");
                    x += 4;
                }
                

                else
                {
                    Console.Write($"{x} ");
                    x += 4;


                }
            }

        }
        static void soal7()

        {
            int n = 7;

            int x = 1;


            for (int i = 1; i <= n; i++)
            {
                x *= 2;
                Console.Write($"{x} ");

            }
        }
        static void soal8()
        {
            int n = 7;

            int x = 1;


            for (int i = 1; i <= n; i++)
            {
                x *= 3;
                Console.Write($"{x} ");

            }
        }
        static void soal9()
        {
            int n = 7;

            int x = 1;


            for (int i = 1; i <= n; i++)
            {
                if (i % 3 == 0)
                {
                    Console.Write($"* ");
                }
                else
                {
                    x *= 4;
                    Console.Write($"{x} ");
                }

            }
        }
        static void soal10()
        {
            int n = 7;

            int x = 1;


            for (int i = 1; i <= n; i++)
            {
                if (i % 4 == 0)
                {
                    Console.Write($"XXX ");
                    x *= 3;
                }
                else
                {
                    x *= 3;

                    Console.Write($"{x} ");
                    
                }

            }
        }
    
    }
}
