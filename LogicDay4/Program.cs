﻿using System;

namespace LogicDay4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("Pilih soal anda = ");
            pilih = int.Parse(Console.ReadLine());
            switch(pilih)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal11();
                    break;
            }

            static void soal1()
            {
                int xinput; 
                int [] y = new int[7];
                Console.Write("Input array = ");
                xinput = int.Parse(Console.ReadLine());
                
                for (int i = 0; i < xinput; i++)
                {
                    Console.Write($"Masukan Data mu ya = ");
                    y[i] = int.Parse(Console.ReadLine());
                }
                Array.Sort(y);
                Console.WriteLine("\nSorted List");
                for(int j=0;j<y.Length;j++)
                {
                    Console.Write(y[j]);
                }
            }

            static void soal2()
            {
                int x,f;
                
                Console.Write("input x = ");
                x = int.Parse(Console.ReadLine());
                
                
                for(int i=1;i<=x;i++)
                {
                    f = 0;

                    for (int j=2; j<=i/2;j++)
                    {
                        if(i%j==0)
                        { 
                            f++;
                            break;
                        }

                    }
                    if (f==0 && i !=1)
                    {
                        Console.Write($"{i} ");
                    }
                    
                    

                    

                }
                Console.ReadKey();

            }
            static void soal3()
            {
                int p,d,m,s;
                

                Console.Write("p = ");
                p = int.Parse(Console.ReadLine());
                Console.Write("d = ");
                d = int.Parse(Console.ReadLine());
                Console.Write("m = ");
                m = int.Parse(Console.ReadLine());
                Console.Write("s = ");
                s = int.Parse(Console.ReadLine());
                int games = 0;
                
                while (p>=m && s>=p)
                {
                    s -= p;
                    p-=d;
                    if(p<m)
                    {
                        p = m;
                        

                    }
                    games++;
                    Console.Write(p + " ");
                }
                
                /*if(p-d<=m)
                {
                    d = m;
                    p -= m;
                    p -= s;
                    games++;
                }*/
                Console.Write($"|||totalnya {games} games");


            }
            static void soal4()
            {

                int f;
                Console.Write("Masukan Angka = ");
                f = int.Parse(Console.ReadLine());
                for (int i = 1; i <= f; i++)
                {
                    for (int j = i; j <= f; j++)
                    {
                        Console.Write(" ");
                    }
                    for(int k=1;k<=i;k++)
                    {
                        Console.Write("*");
                    }
                    Console.Write("\n");
                }
            }
            static void soal5()
            {
                int total=0;
                string infut,kata;
                Console.Write("Infut = ");
                infut = Console.ReadLine();
                if(infut.Length%3==0)
                {
                    for(int i=0;i<infut.Length;i+=3)
                    {
                        kata = infut.Substring(i, 3);
                        Console.WriteLine(kata);
                        if(kata.ToUpper()!="SOS")
                        {
                            total++;
                            
                        }
                    }
                    Console.Write(total);
                }
                
                else
                {

                }

                
            }
            static void soal11()
            {
                string input = "2,5,4,1,3";
                string[] soal = input.Split(",");

                for (int i = 0; i < 5; i++)
                {
                    int k = 1;
                    while ((int.Parse(soal[i])) != k)
                    {

                        i++;
                        
                    }
                    Console.Write($"{k}");
                    k += 1;
                }
                
                

            }

        }
    }
}
