﻿using System;
using System.Linq;

namespace LogicDay5
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("Masukan Soal Anda (1/2/3/4) = ");
            pilih = int.Parse(Console.ReadLine());
            switch(pilih)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
            }
            static void soal1()
            {
                Console.Write("Contoh input : 07:05:45PM ");
                string a = "12:05:45AM";
                int jam = int.Parse(a.Substring(0, 2));
                string ampm = a.Substring(8, 2);
                if (ampm == "PM")
                {
                    jam+=12;
                    if (jam < 24)
                    {
                        Console.WriteLine($"{jam}" + $"{a.Substring(2, 6)}");
                    }
                    else 
                    {
                        if(jam>=24)
                        {
                            int lewih = 0;
                            Console.WriteLine($"0{jam-24}" + $"{a.Substring(2, 6)}");
                        }
                    }
                }
                else if(ampm == "AM")
                {
                    if(ampm=="AM")
                    {
                        Console.WriteLine($"{jam}" + $"{a.Substring(2, 6)}");
                    }

                }
            }
            static void soal2()
            {

            }
            static void soal3()
            {
                int[,] dataku =
                {
                    {11,2,4 },{4,5,6},{10,8,-12}
                };
                int d1 = 0;
                int d2 = 0;
                for(int i=0;i<3;i++)
                {
                    for(int j=0;j<3;j++)
                    {
                        if(j==i)
                        {
                            d1+=dataku[i,j];
                        }
                        if(j==2-i)
                        {
                            d2 += dataku[i, j];
                        }
                    }
                }
                int d = d1 - d2;
                Console.Write($"d adalah {d}");
            }

            static void soal4()
            {
                Console.Write("Masukan lilin mu = ");
                string[] lilin = Console.ReadLine().Split(" ");
                int[] Lilin = Array.ConvertAll(lilin, int.Parse);
                Console.Write($"lilin yang paling tinggi adalah {Lilin.Max()} ");
                int games = 0;
                for (int i = 0; i < Lilin.Length; i++)
                {
                    if (Lilin[i] ==Lilin.Max())
                    {
                        games++;
                    }
                }
                Console.WriteLine($"lilin yang berhasil ditiup {games} lilin");

            }
        }
    }
}
