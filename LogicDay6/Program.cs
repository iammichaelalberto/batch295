﻿using System;
using System.Linq;
using System.Collections.Generic;
namespace LogicDay6
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.Write(" Pilih Soal Logic Day 6 = ");
            int pilih = int.Parse(Console.ReadLine());
            bool ulangi = true;
            while (ulangi)
            {
                switch (pilih)
                {
                    case 4:
                        soal4();
                        break;
                    case 3:
                        soal3();
                        break;
                    case 2:
                        soal2();
                        break;
                    case 1:
                        soal1();
                        break;
                    case 44:
                        soal44();
                        break;

                }

                Console.WriteLine("lagi ? y/n = ");
                string pilihh = Console.ReadLine();
                if (pilihh == "n")
                {

                    ulangi = false;
                }
                Console.ReadKey();



            }
        }
        static void soal4()
        {
            /*Console.Write("Input : ");
            string[] inp = Console.ReadLine().Split(" ");*/
            string inp = "simple,case";
            string[] inpp = inp.Split(",");
            string vokal = "a,i,u,e,o";
            string[] vokall = vokal.Split(",");
            for (int i = 0; i < inpp.Length; i++)
            {
                for (int j = 0; j < vokall.Length; j++)
                {
                    /* if ((inpp[i].IndexOf(inpp[i])) == vokall[j])
                     {
                         Console.Write($"{vokall[j]}");
                     }*/
                }
            }
            /*Console.Write($"{inpp[1].IndexOf(inpp[1].ToCharArray)}");*/
        }
        static void soal3()
        {
            string[] p = "1 3 1 4 6 2 1 1 3 5 2 3 1 1 1 1 5 2 3 1 3 5 4 3 2 5".Split(" ");
            int[] pp = Array.ConvertAll(p, int.Parse);
            string[] k = "d i a".Split(" ");
            string[] a = "a b c d e f g h i j k l m n o p q r s t u v w x y z".Split(" ");
            Console.Write($"{p[1]}");
            foreach (string i in k)
            {
                foreach (string j in a)
                {
                    if (i.Equals(j))
                    {


                        Console.Write($"");///mau ngeprint index p[j]
                    }
                }
            }

        }
        static void soal2()
        {

            Console.WriteLine("Masukan kalimat anda : ");
            string msgString = Console.ReadLine();
            msgString = msgString.ToLower();
            char[] secretMessage = msgString.ToCharArray();
            Console.Write("Masukan Rotate = ");
            int rot = int.Parse(Console.ReadLine());
            Encrypt(secretMessage, rot);
            string secret = Encrypt(secretMessage, rot);
            Console.WriteLine(secret);


        }
        static string Encrypt(char[] secretMessage, int key)
        {
            char[] alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

            int length = secretMessage.Length;
            //Console.WriteLine(length);
            char[] encryptedMessage = new char[length];
            for (int i = 0; i < secretMessage.Length; i++)
            {
                var letter = secretMessage[i];
                int index = Array.IndexOf(alphabet, letter);
                int newIndex = (key + index) % 26;
                char newLetter = alphabet[newIndex];
                encryptedMessage[i] = newLetter;
                //Console.WriteLine($"{letter}, {index}");
            }

            string enMessage = String.Join("", encryptedMessage);
            //Console.WriteLine(enMessage);
            return enMessage;
        }


        static void soal1()
        {
            string[] ud = "U D U D D U U U D U D U D U U D U U D D D D D U D U D D D D U U D D U D D U U U U D U U D U D D D D U D U D U U U D D D U U U D U D D U U D D D U U D D U D D D U D U U D U U D U U D U D D D U U U U U".Split(" ");
            /*Console.WriteLine(ud[9]);*/
            int batas = 0;
            int lembah = 0;
            for(int i = 0; i < ud.Length; i++)
            {
                if (ud[i] == "D")
                {
                    batas--;
                }
                else if(ud[i]=="U")
                {
                    batas++;
                }
                if(ud[i]=="D" && batas == -1)
                {
                    lembah++;
                }

            }
            Console.Write($"{lembah}");
            


        }
        static void soal44()
        {
            String kalimat;
            Console.Write("Input Kalimat : ");
            kalimat = Console.ReadLine().ToLower();
            Console.WriteLine();
            Console.Write("Huruf Vocal    = ");
            for (int i = 0; i < kalimat.Length; i++)
            {
                if (kalimat[i] == 'a' || kalimat[i] == 'e' || kalimat[i] == 'i' || kalimat[i] == 'u' || kalimat[i] == 'o')
                {
                    Console.Write(kalimat[i] + " ");
                }
            }
            Console.WriteLine();
            Console.Write("Huruf Konsonan = ");
            for (int i = 0; i < kalimat.Length; i++)
            {
                if (kalimat[i] == 'a' || kalimat[i] == 'e' || kalimat[i] == 'i' || kalimat[i] == 'u' || kalimat[i] == 'o')
                {
                    continue;
                }
                else
                {
                    if (char.IsWhiteSpace(kalimat[i]))
                    {
                        continue;
                    }
                    else
                    {
                        Console.Write(kalimat[i] + " ");
                    }

                }
            }
            Console.ReadKey();
        }
    }
}
