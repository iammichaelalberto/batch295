﻿using System;


namespace LogicDay7
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukan Soal nomor berapa = ");
            int pilih = int.Parse(Console.ReadLine());
            switch (pilih)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal6();
                    break;
                case 7:
                    soal7();
                    break;
            }


            Console.ReadKey();
            /*int x = 101 / 2;
            x %= 5;
            Console.WriteLine(x);*/
        }
        static void Date()
        {
            DateTime date = new DateTime();
            Console.WriteLine(date);

            DateTime date1 = new DateTime(2022, 07, 08, 10, 30, 00);
            Console.WriteLine(date1);

            DateTime date2 = new DateTime(2021, 05, 05);
            Console.WriteLine(date2);

            DateTime dateNow = DateTime.Now;
            Console.WriteLine(dateNow);

            string d = "29/12/2022";
            DateTime convertStringKeTime = DateTime.Parse(d);
            Console.WriteLine(convertStringKeTime);

            int year = dateNow.Year;
            int menit = dateNow.Minute;
            var hari = dateNow.DayOfWeek;
            var harii = (int)dateNow.DayOfWeek;
            Console.WriteLine($"{year} {menit} {hari} {harii}");

            TimeSpan interval = date2 - dateNow;
            Console.WriteLine($"{interval}");

            dateNow = dateNow.AddDays(1);
            Console.WriteLine($"{dateNow}");


        }
        static void soal6()
        {
            Console.Write("Masukan x! = ");
            int x = int.Parse(Console.ReadLine());

            int fak =1;

            for (int i = 1; i <= x; i++)
            {
                 fak *=i;
                
                

            }
            Console.Write(fak);
        }
        static void soal5()
        {
            /*int titikAwal = 1;
            int custumorKe = 3;*/
            Console.Write("Titik awal #1 sampai 4 = ");
            int titikAwal = int.Parse(Console.ReadLine());
            Console.Write("Titik akhir = ");
            int custumorKe = int.Parse(Console.ReadLine());
            int totalJarak = 0;
            int[] jarak = Array.ConvertAll(("2000,500,1500,300").Split(","), int.Parse);
            for (int i = titikAwal-1; i < custumorKe; i++)
            {
                totalJarak += jarak[i];
            }
            int bensin = 0;                //1 liter 2500 M
            bensin = totalJarak / 2500;

            if (totalJarak % 2500 !=0)
            {
                bensin++;
            }
            Console.WriteLine($"Total jarak anda adalah {totalJarak} M");
            Console.WriteLine($"Bensin anda setidaknya memiliki {bensin} liter");


        }
        static void soal4()
        {
            Console.Write("Input tanggal dan jam (mulai) : ");
            string mulai = Console.ReadLine();
            DateTime dataMulai = DateTime.Parse(mulai);

            Console.Write("Input durasi main : ");
            int durasi = int.Parse(Console.ReadLine());

            Console.Write("Input tambah durasi main : ");
            int tambah = int.Parse(Console.ReadLine());


            TimeSpan jumlah = dataMulai.AddHours(durasi) - dataMulai;
            int harga = jumlah.Hours * 3500;
            Console.WriteLine($"Akan selesai pada {dataMulai.AddHours(durasi)} dan harga yang harus dibayar adalah Rp.{harga}");

            TimeSpan jumlahTambah = dataMulai.AddHours(durasi + tambah) - dataMulai;
            int hargaTambah = jumlahTambah.Hours * 3500;
            Console.WriteLine($"Jika ditambah {tambah} jam maka akan selesai pada {dataMulai.AddHours(durasi + tambah)} dan harga yang harus dibayar sebanyak {hargaTambah}");

        }
        static void soal7()
        {

            /*int point = 20;
            int taruhan = 19;
            string tebakan = "U";*/   //U itu lebih dari 5 /// D kurang dari sama dengan 5

            
            bool ulangi = true;
            while (ulangi)
            {
                Console.Write("Point awal = ");
                int point = int.Parse(Console.ReadLine());

                Console.Write("Taruhan anda ? = ");
                int taruhan = int.Parse(Console.ReadLine());
                Console.Write("Tebakan anda ? U/D = ");
                string tebakan = Console.ReadLine();

                Random randomm = new Random();
                int tebak = randomm.Next(0, 10);
                Console.WriteLine("");
                Console.WriteLine($"angka judi yang keluar adalah {tebak}");
                


                if ((tebak > 5 && tebakan == "U") || (tebak <= 5 && tebakan == "D"))
                {
                    point += taruhan;
                    Console.WriteLine($"You Win");
                }
                else if ((tebak > 5 && tebakan == "D") || (tebak <= 5 && tebakan == "U"))
                {
                    point -= taruhan;
                    Console.WriteLine($"You Lose");
                }



                Console.WriteLine($"point anda sekrang {point}");
                Console.WriteLine("Main lagi yuk tapi inget-inget point akhirnya y/n =");
                string input = Console.ReadLine();
                if (input.ToLower() == "n")
                {
                    ulangi = false;
                }
                if (point <= 0 )
                {
                    Console.WriteLine($" Game over,Beli point biar bisa main lagi");
                    ulangi = false;
                }
                Console.Clear();
            }



        }
        static void soal2()
        {
            int total = 0;

            Console.Write("Masukkan Tanggal Meminjam Buku = ");
            string tanggal_meminjam = Console.ReadLine();
            DateTime pinjam = DateTime.Parse(tanggal_meminjam);

            Console.Write("Masukkan Lama Peminjaman = ");
            int lama_peminjaman = int.Parse(Console.ReadLine());

            Console.WriteLine($"Tanggal Seharusnya Mengembalikan = {pinjam = pinjam.AddDays(lama_peminjaman)}");

            Console.Write("Masukkan Tanggal Mengembalikan Buku = ");
            string tanggal_mengembalikan = Console.ReadLine();
            DateTime kembalikan = DateTime.Parse(tanggal_mengembalikan);

            if (kembalikan > pinjam.AddDays(lama_peminjaman))
            {
                TimeSpan interval = kembalikan - pinjam.AddDays(lama_peminjaman);
                int Interval = (int)interval.TotalDays;
                total = Interval * 500;
                Console.WriteLine($"Denda = Rp{total}");
            }
            else
            {
                Console.WriteLine("Denda = Rp.0");
            }
        }
        static void soal1()
        {
            Console.Write("Input Tanggal (dd/mm/yyyy) = ");
            string input = Console.ReadLine();
            DateTime dateinput = DateTime.Parse(input);

            Console.Write("Input Jam Masuk = ");
            string masuk = Console.ReadLine();
            DateTime datemasuk = DateTime.Parse(masuk);

            Console.Write("Input Jam Keluar = ");
            string keluar = Console.ReadLine();
            DateTime datekeluar = DateTime.Parse(keluar);

            TimeSpan Hour = datekeluar - datemasuk;
            Console.WriteLine($"Lama Parkir: {Hour} jam");

            int total = Hour.Hours * 3000;
            Console.WriteLine($"Total yang harus di bayar:  Rp.{total}");


        }
        static void soal3()
        {

        }

    }
}
