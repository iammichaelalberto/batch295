﻿using System;

namespace PRDay4UpToDate
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("Masukan Soal 1-4=5 = ");
            pilih = int.Parse(Console.ReadLine());
            switch (pilih)
            {
                case 1:
                    soal1versiforeach();
                    break;
                case 2:
                    soal2versiforeach();
                    break;
                case 4:
                    soal4();
                    break;
                case 3:
                    soal3();
                    break;
            }
            
        }
        static void soal1()
        {
            string x = "Aku Sayang Kamu";

            string[] y = x.Split(" ");
            for (int i = 0; i < y.Length; i++)
            {
                for (int j = 0; j < y[i].Length; j++)
                {
                    if (j == 0 || j == y[i].Length - 1)
                    {
                        Console.Write($"{y[i]}");
                    }
                    else
                    {
                        Console.Write($"*");
                    }



                }
            }
        }
        static void soal1versiforeach()
        {
            string x = "Aku Sayang Kamu";

            string[] y = x.Split(" ");
            foreach (string a  in y)
            {
                for (int j = 0; j < a.Length; j++)
                {
                    if (j == 0 || j == a.Length - 1)
                    {
                        Console.Write($"{a[j]}");
                    }
                    else
                    {
                        Console.Write($"*");
                    }



                }
                Console.Write($" ");
            }
        }
        static void soal2versiforeach()
        {
            string x = "Aku Sayang Kamu";

            string[] y = x.Split(" ");
            foreach (string a in y)
            {
                for (int j = 0; j < a.Length; j++)
                {
                    if (j == 0 || j == a.Length - 1)
                    {
                        Console.Write($"*");
                    }
                    else
                    {
                        Console.Write($"{a[j]}");
                        
                    }



                }
                Console.Write($" ");
            }
        }
       

        static void soal2()
        {
            string x = "Aku Mau Makan";

            string[] y = x.Split(" ");
            for (int i = 0; i < y.Length; i++)
            {
                for (int j = 0; j < y[i].Length; j++)
                {
                    if (j == 0 || j == y[i].Length - 1)
                    {
                        Console.Write($"*");
                    }
                    else
                        Console.Write($"{y[i]}");




                }
            }
        }
        static void soal3()
        {
            string input;

            Console.Write("kalimat = ");
            input = Console.ReadLine().ToLower();
            string harikebalikan="";

            for (int i = input.Length - 1; i >=0 ; i--)
            {
                harikebalikan += input[i].ToString();
            }
            if(input==harikebalikan)
            {
                Console.Write($"Yes");
            }
            else
            
                Console.Write($"No");
            


        }
        static void soal44()
        {
            string x = "35,40,50,20";
            string y = "40,30,45,10";

            
            string[] z = x.Split(",");
            string[] a = y.Split(",");

            int[] celana = new int[z.Length];
            int[] baju = new int[a.Length];

            int uangandi = 78;

            foreach (string b in z)
            {
                foreach (string c in a)
                {
                    
                    for (int i = 0; i < 1; i++)
                    {
                        celana[i] = int.Parse(b);
                        baju[i] = int.Parse(c);
                        
                            
                            int[] total = new int[z.Length];
                            total[i] = celana[i] + baju[i];

                            Console.Write($"{total[i]} ");
                        
                        
                    }
                }
                
            }
        }
        static void soal4()
        {
            Console.Write("Masukan input mu ya = ");
            int input = int.Parse(Console.ReadLine());

            Console.Write("Masukan harga celana mu ya = ");
            string[] celana = Console.ReadLine().Split(",");
            int[] Celana = Array.ConvertAll(celana, int.Parse);

            Console.Write("Masukan Harga Baju = ");
            string[] baju = Console.ReadLine().Split(",");
            int[] Baju = Array.ConvertAll(baju, int.Parse);

            int maxValue = 0;
            int[] jumlah = new int[Baju.Length];
            for(int i=0;i<Baju.Length;i++)
            {
                int temp = Celana[i] + Baju[i];
                if(temp<=input)
                {
                    jumlah[i] = temp;
                    if(maxValue<temp)
                    {
                        maxValue = temp;
                    }
                }
            }
            Console.Write($"kombinasi baju dan celana termahal anda = {maxValue}");


        }

    }
}
