﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;


namespace PRDay6Uptodate
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal5();
            //soal2();
            //soal1();
            soal3();
        }
        static void soal2()
        {
            Console.Write("Masukan kalimat = ");
            string value = Console.ReadLine();
            byte[] asciiBytes = Encoding.ASCII.GetBytes(value);

            Console.Write("Masukan Rotasi = ");
            int rot = int.Parse(Console.ReadLine());

            for (int i = 0; i < asciiBytes.Length; i++)
            {
                if (asciiBytes[i] >= 97 && asciiBytes[i] <= 122) //huruf kecil
                {
                    byte[] hasilRotasi = BitConverter.GetBytes((((asciiBytes[i] + rot) - 97) % 25) + 97);
                    var str = System.Text.Encoding.Default.GetString(hasilRotasi);
                    Console.Write($"{str}");
                }
                else if (asciiBytes[i] >= 65 && asciiBytes[i] <= 90) //huruf kapital
                {
                    byte[] hasilRotasi = BitConverter.GetBytes((((asciiBytes[i] + rot) - 65) % 25) + 65);
                    var str = System.Text.Encoding.Default.GetString(hasilRotasi);
                    Console.Write($"{str}");
                }
                else
                {
                    byte[] hasilRotasi = BitConverter.GetBytes(asciiBytes[i]);
                    var str = System.Text.Encoding.Default.GetString(hasilRotasi);
                    Console.Write($"{str}");
                }
            }

        }
        static void soal5()
        {
            /* string[] input = "A b 1".Split(" ");
            Console.Write($"{byte(9)}");*/

            /*string value = "abcaade";
            // Convert the string into a byte[].
            byte[] asciiBytes = Encoding.ASCII.GetBytes(value);*/
            /*Console.Write(asciiBytes[0]+ " ");*/

            /*var str = System.Text.Encoding.Default.GetString(asciiBytes);
            *//*Console.Write(str+" ");*//*
            string result = System.Text.Encoding.UTF8.GetString(asciiBytes);*/
            /*Console.Write(result);*/
            /*Console.Write(asciiBytes.Length);*/

            Console.Write("Masukan Password anda = ");
            string value = Console.ReadLine();
            byte[] asciiBytes = Encoding.ASCII.GetBytes(value);
            int kapital = 0;
            int hurufKecil = 0;
            int angka = 0;
            int karakterKhusus = 0;
            for (int i = 0; i < asciiBytes.Length; i++)
            {
                if (asciiBytes[i] >= 65 && asciiBytes[i] <= 90)   //65 sampai 90 itu huruF KAPITAL
                {
                    kapital++;
                }
                else if (asciiBytes[i] >= 97 && asciiBytes[i] <= 122)    //97 sampai 122 huruf kecil
                {
                    hurufKecil++;
                }
                else if (asciiBytes[i] >= 48 && asciiBytes[i] <= 57)       //48 sampai 57 angka(0 sampai 9)
                {
                    angka++;
                }
                else if ((asciiBytes[i] >= 35 && asciiBytes[i] <= 38) || asciiBytes[i] == 33 || asciiBytes[i] == 64 || asciiBytes[i] == 94 || (asciiBytes[i] >= 40 && asciiBytes[i] <= 43) || asciiBytes[i] == 45)         // 35 sampai 28 itu # $ % & //33 itu ! // 64 itu @
                {
                    karakterKhusus++;                                                                                   //95 itu ^////45 itu -// 40 sampai 43 itu ( ) * +
                }


            }
            if (value.Length <= 5)
            {
                Console.Write($"Pesan kurang panjang pa, serius lah pa");
                Console.Write("\n");
            }
            if (angka <= 0)
            {
                Console.Write($"fasword anda tidak ada angka pa,plese deh ");
                Console.Write("\n");
            }
            if (kapital <= 0)
            {
                Console.Write($"fasword anda tidak ada huruf besar pa,plese deh ");
                Console.Write("\n");
            }
            if (hurufKecil <= 0)
            {
                Console.Write($"fasword anda tidak ada huruf kecil pa, ayolah ");
                Console.Write("\n");
            }
            if (karakterKhusus <= 0)
            {
                Console.Write($"fasword anda tidak ada karakter khusus pa, dahlah ");
                Console.Write("\n");
            }
            if(value.Length > 5 && angka > 0 && karakterKhusus>0 && hurufKecil>0 && kapital>0)
            {
                Console.Write($"Password sudah seterong ");
            }
        }
        static void soal1()
        {
            string[] ud = "U D U D D U U U D U D U D U U D U U D D D D D U D U D D D D U U D D U D D U U U U D U U D U D D D D U D U D U U U D D D U U U D U D D U U D D D U U D D U D D D U D U U D U U D U U D U D D D U U U U U".Split(" ");
            /*Console.WriteLine(ud[9]);*/
            int batas = 0;
            int lembah = 0;
            for (int i = 0; i < ud.Length; i++)
            {
                if (ud[i] == "D")
                {
                    batas--;
                }
                else if (ud[i] == "U")
                {
                    batas++;
                }
                if (ud[i] == "D" && batas == -1)
                {
                    lembah++;
                }

            }
            Console.Write($"{lembah}");



        }
        static void soal3()
        {

            /*string value = "dia";
            byte[] asciiBytes = Encoding.ASCII.GetBytes(value);
            string[] p = "1 3 1 4 6 2 1 1 3 5 2 3 1 1 1 1 5 2 3 1 3 5 4 3 2 5".Split(" ");*/
            /* for (int i = 0; i < value.Length; i++)
             {
                 int indekAsciiBytes = Array.IndexOf(asciiBytes, 1);
                 Console.Write(indekAsciiBytes);

             }
             for (int j = 0; j < p.Length; j++)
             {
                 int[] indekp = Array.IndexOf(asciiBytes, j);
             }
             if (indekAsciiBytes[] == indekp[])
             {

             }*/
            Console.WriteLine("Masukan tinggi = ");
            int[] tinggi = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            Console.WriteLine("Masukan kalimat");
            string kalimat = Console.ReadLine();
            string alfabet = "abcdefghijklmnopqrstuvwxyz";

            List<int> simpan = new List<int>();
            for (int i = 0;i < kalimat.Length;i++)
            {
                char huruf = kalimat[i];
                int index = alfabet.IndexOf(huruf);
                simpan.Add(tinggi[index]);
                Console.WriteLine($"index alfbet {huruf}={index} maka ke index {index}di element tinggi {tinggi[index]}");
            }
            int len = kalimat.Length;
            int max = simpan.Max();
            int hasil = len * max;
            Console.WriteLine($"{len} * {max} = {hasil}");


        }
        static void soal4()
        {

        }

    }
}
