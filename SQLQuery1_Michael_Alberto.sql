--create database batch_295

--create table karyawan(
--	id int primary key identity(1,1),
--	nama varchar(100) not null,
--	alamat varchar(225) not null,
--	email varchar(50),
--	gender varchar(1),
--	no_hp varchar(15)
--)

--alter table karyawan add status varchar(20)
--alter table karyawan alter column status varchar(50)
--alter table karyawan drop column status

select * from karyawan
insert into karyawan 
(nama,alamat,email,gender,no_hp) values
--('Hanis','jakarta barat1','hanis@gmail1.com','W','081234567789'),
('Hanis1','jakarta barat2','hanis@gmail2.com','P','081234567789'),
('Hanis2','jakarta barat3','hanis@gmail4.com','W','081234567789'),
('Hanis3','jakarta barat4','hanis@gmail5.com','P','081234567789'),
('Hanis4','jakarta barat5','hanis@gmail6.com','P','081234567789'),
('Hanis5','jakarta barat6','hanis@gmail7.com','P','081234567789'),
('Hanis6','jakarta barat7','hanis@gmail8.com','W','081234567789'),
('Hanis7','jakarta barat8','hanis@gmail9.com','W','081234567789'),
('Hanis8','jakarta barat9','hanis@gmail10.com','P','081234567789'),
('Hanis9','jakarta barat10','hanis@gmail11.com','W','081234567789')

delete from karyawan where id=1
update karyawan set alamat = 'jakarta timur' where id=2
update karyawan set alamat = 'jakarta timur' where id>=5 and id<=7

select top 3 * from karyawan order by id desc

select top 3 * from karyawan where nama like 'Ha%' order by id asc

select top 3 * from karyawan where nama not like '%Ha%' order by id asc

select COUnt (gender) from karyawan

select COUnt (gender) from karyawan group by gender

select COUnt (gender) as jml_gender, gender from karyawan group by gender

select COUnt (gender) as jml_gender, gender,nama from karyawan group by gender,nama

select COUnt (gender) as jml_gender, gender 
from karyawan where gender ='P'
group by gender

select COUnt (gender) as jml_gender, gender 
from karyawan
group by gender
having Count(gender)>=5


create table cuti(

id int primary key identity (1,1),
id_karyawan int not null, 
kouta_cuti int not null, 
ambil_cuti int not null, 
alasan varchar (50)
)
insert into cuti
(id_karyawan,kouta_cuti,ambil_cuti,alasan) values
(2,12,2,'liburan'),
(3,12,1,'gering'),
(4,12,3,'pulkam'),
(2,12,1,'turnamen')

select * from cuti


select * from karyawan as k
join cuti as c
on k.id=c.id_karyawan

select k.nama, c.kouta_cuti, c.ambil_cuti, c.alasan 
from karyawan as k
inner join cuti as c
on k.id=c.id_karyawan

select k.nama, c.kouta_cuti, c.ambil_cuti, c.alasan 
from karyawan as k
left join cuti as c
on k.id=c.id_karyawan

select k.nama, c.kouta_cuti, c.ambil_cuti, c.alasan, c.kouta_cuti-c.ambil_cuti as sisa_suci 
from karyawan as k
right join cuti as c
on k.id=c.id_karyawan

select k.nama, c.kouta_cuti, c.ambil_cuti, c.alasan, c.kouta_cuti-c.ambil_cuti as sisa_suci 
from karyawan as k
right join cuti as c
on k.id=c.id_karyawan
where c.id is null

select k.nama, c.kouta_cuti, c.ambil_cuti, c.alasan, c.kouta_cuti-c.ambil_cuti as sisa_suci 
from karyawan as k
full outer join cuti as c
on k.id = c.id_karyawan
where k.id is null or c.id is null


create view vw_pengajuan_cuti as
select k.nama, c.kouta_cuti, c.ambil_cuti, c.alasan, c.kouta_cuti-c.ambil_cuti as sisa_suci 
from karyawan as k
inner join cuti as c
on k.id = c.id_karyawan

select * from vw_pengajuan_cuti
where sisa_cuti>10
order by nama










