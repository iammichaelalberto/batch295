﻿using System;

namespace Tugas2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int pilih;
            Console.Write("Masukan nomor = ");
            pilih = int.Parse(Console.ReadLine());
            switch(pilih)
            {
                case 1:
                    soal1();
                    break;
                case 2:
                    soal2();
                    break;
                case 3:
                    soal3();
                    break;
                case 4:
                    soal4();
                    break;
                case 5:
                    soal5();
                    break;
                case 6:
                    soal6();
                    break;
            }

        }
        static void soal1()
        {
            double x;
            double y;

            Console.Write("Input = ");
            x = int.Parse(Console.ReadLine());
            int i = 2;
            while (i <= x)
            {
                if (x % i == 0)
                {
                    y = x / i;
                    Console.WriteLine($"{x}/{i}={y}");
                    x = y;
                    
                }
                else
                {
                    i++;
                    
                }
            }

       
    }
        static void soal2()
        {
            int angka=1;
            int batasatasi, batasatasj;
            Console.Write("Masukan batas atas i = ");
            batasatasi = int.Parse(Console.ReadLine());
            Console.Write("Masukan batas atas j = ");
            batasatasj = int.Parse(Console.ReadLine());
            
            for (int i=1;i<=batasatasi;i++)
            {
                for(int j=1;j<=batasatasj;j++)
                {
                    if (i == 1)
                    {
                        Console.Write($"{angka}");
                        angka += 1;
                    }
                    else if (i == batasatasi)
                    {
                        angka -= 1;
                        Console.Write($"{angka}");
                        
                    }
                    else if ((i>1 && i<batasatasi && j==1) ||(i > 1 && i < batasatasi && j ==batasatasj))
                    {
                        Console.Write($"*");
                    }
                    else if(i==j && i>1 && i<batasatasi && j>1 && j<batasatasj )
                    {
                        Console.Write($"#");
                    }
                    else
                    {
                        Console.Write($" ");
                    }
                    


                }

                Console.Write("\n");

            }
            
        }
        static void soal3()
        {
            int n = 7;
            int x = 3;
            for(int i=1;i<=n;i++)
            {
                if(i%2==0)
                {
                    Console.Write($"* ");
                }
                else
                {
                    Console.Write($" {x}");
                }
                x *= 3;
            }
        }
        static void soal4()
        {
            int x = 5;
            
            for(int i=1;i<=7;i++)
            {
                if(i%2==1)
                {
                    Console.Write($"-{x} ");
                    
                    x += 5;
                    
                }
                else
                {
                    Console.Write($"{x} ");
                    x += 5;
                    
                }
            }
        }
        static void soal5()
        {
            int a, b,c,n;
            a = 1;
            b = 1;
            Console.Write("Masukan batas atas n = ");
            n = int.Parse(Console.ReadLine());
            for (int i=2;i<=n+1;i++)
            {
                c = a + b;
                Console.Write($"{a},");
                a = b;
                b = c;


            }
        }
        static void soal6()
        {
            int a, b, c, d;
            a = 1;
            b = 1;
            c = 1;
            
            for (int i = 1; i <= 7; i++)
            {
                
                d = a + b + c;
                Console.Write($"{a},");
                a = b;
                b = c;
                c = d;
                


            }
        }

        
    }
}
