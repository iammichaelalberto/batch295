﻿using System;
using System.Collections.Generic;

namespace soal4
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukan berapa buah bilangan genap yang anda inginkan : ");
            int n = int.Parse(Console.ReadLine());
            //int n = 10;
            List<int> x = new List<int>();
            List<int> y = new List<int>();
            List<int> z = new List<int>();
            for (int i = 1; i <= n*3000000; i++)
            {
                x.Add(i*3);
            }
            //Console.Write(String.Join(",",x));
            
            for(int k=0; k<n*2; k++)
            {
                if(x[k] % 2 == 0)
                {
                    y.Add(x[k]);
                }
            }
            for(int kk=0;kk<n*2; kk++)
            {
                if(x[kk] %4==0 && x[kk] % 8 == 0)
                {
                    z.Add(x[kk]);
                }
            }
            Console.WriteLine("");
            Console.WriteLine("Bilangan Genap nya dibawah");
            Console.Write(String.Join(",",y));
            Console.Write("\n");
            Console.Write(String.Join(",",z));

        }
    }
}
