﻿using System;

namespace soal6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Masukan Perjalan = ");
            string[] ud = Console.ReadLine().Split(" ");
            //string[] ud = "N N T N N N T T T T T N T T T N T N".Split(" ");
            
            int batas = 0;
            int lembah = 0;
            int gunung = 0;
            //int jumlah = 0;
            for (int i = 0; i < ud.Length; i++)
            {
                if (ud[i] == "T")
                {
                    batas--;
                    //jumlah--;
                }
                else if (ud[i] == "N")
                {
                    batas++;
                    //jumlah--;
                }
                if (ud[i] == "T" && batas == -1)
                {
                    lembah++;
                }
                else if(ud[i]== "N" && batas == 1)
                {
                    gunung++;
                }

            }
            Console.Write($"Lembah = {lembah}");
            Console.Write($"\nGunung = {gunung}");
        }
    }
}
